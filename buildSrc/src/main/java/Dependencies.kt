/** */
object Build {
    const val BUILD_TOOL_VERSION = "30.0.2"
}
/** */
object KotlinVersion {
    const val KOTLIN_VERSION = "1.4.20"
}
/** */
object Sdk {
    const val MIN_SDK_VERSION = 24
    const val TARGET_SDK_VERSION = 30
    const val COMPILE_SDK_VERSION = 30
}
/** */
object Plugins {
    const val ANDROID_APP = "com.android.application"
    const val KOTLIN_ANDROID = "kotlin-android"
    const val KOTLIN_PARCELIZE = "kotlin-parcelize"
    const val NAVIGATION_SAFEARGS = "androidx.navigation.safeargs.kotlin"
    const val KOTLIN_KAPT = "kotlin-kapt"

}
/** */
object Versions {
    /* TOOLS BUILD */
    const val TOOLS_BUILD_VERSION = "4.1.3"
    /* ANDROID X */
    const val CONSTRAINT_LAYOUT_VERSION = "2.0.2"
    const val CORE_KTX_VERSION = "1.3.2"
    const val APPCOMPAT_VERSION = "1.2.0"
    const val LEGACY_SUPPORT_VERSION = "1.0.0"
    /* DEBUG */
    const val TIMBER_VERSION = "4.7.1"
    /* RETROFIT */
    const val RETROFIT_VERSION = "2.9.0"
    const val RETROFIT_OKHTTP_VERSION = "4.8.0"
    /* MOSHI */
    const val MOSHI_VERSION = "1.9.3"
    /* GOOGLE */
    const val MATERIAL_COMPONENTS_VERSION = "1.2.0-alpha06"
    /* KOIN */
    const val KOIN_VERSION = "1.0.2"
    /* LIFECYCLE */
    const val LIFECYCLE_VERSION = "2.2.0"
    /* COROUTINES */
    const val KOTLIN_COROUTINES_VERSION = "1.3.7"
    /* TESTING */
    const val NAVIGATION_VERSION = "2.3.5"
    /* COIL */
    const val COIL_VERSION = "1.0.0-rc3"
    /* LOTTIE */
    const val LOTTIE_VERSION = "3.4.1"
    /* ANDROID TESTING LIBS */
    const val ANDROIDX_TEST_VERSION = "1.2.0"
    const val ANDROIDX_TEST_EXT_VERSION = "1.1.2"
    const val ESPRESSO_CORE_VERSION = "3.3.0"
    const val JUNIT_VERSION = "4.13.1"
    /* ROOM */
    const val ROOM_VERSION = "2.2.5"
    /* SNACKBAR */
    const val SNACKBAR_VERSION = "1.1.2"
}

/** */
object Dependencies {
    /** KOTLIN JETBREAINS */
    const val TOOLS_BUILD = "com.android.tools.build:gradle:${Versions.TOOLS_BUILD_VERSION}"
    const val JETBRAINS_KOTLIN_GRADLE_PLUGIN ="org.jetbrains.kotlin:kotlin-gradle-plugin:${KotlinVersion.KOTLIN_VERSION}"
    const val JET_BRAINS_KOTLIN =
        "org.jetbrains.kotlin:kotlin-stdlib:${KotlinVersion.KOTLIN_VERSION}"
    /** ANDROIDX */
    const val ANDROIDX_APPCOMPAT = "androidx.appcompat:appcompat:${Versions.APPCOMPAT_VERSION}"
    const val ANDROIDX_CORE_KTX = "androidx.core:core-ktx:${Versions.CORE_KTX_VERSION}"
    const val CONSTRAINT_LAYOUT =
        "androidx.constraintlayout:constraintlayout:${Versions.CONSTRAINT_LAYOUT_VERSION}"
    const val LEGACY_SUPPORT = "androidx.legacy:legacy-support-v4:${Versions.LEGACY_SUPPORT_VERSION}"
    /** NAVIGATION COMPONENTS */
    const val NAVIGATION_FRAGMENT_KTX = "androidx.navigation:navigation-fragment-ktx:${Versions.NAVIGATION_VERSION}"
    const val NAVIGATION_UI_KTX = "androidx.navigation:navigation-ui-ktx:${Versions.NAVIGATION_VERSION}"
    const val NAVIGATION_FEATURES_FRAGMENT = "androidx.navigation:navigation-dynamic-features-fragment:${Versions.NAVIGATION_VERSION}"
    /** DEBUG */
    const val TIMBER = "com.jakewharton.timber:timber:${Versions.TIMBER_VERSION}"
    /** RETROFIT */
    //implementation 'com.google.code.gson:gson:2.8.5'
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${Versions.RETROFIT_VERSION}"
    const val RETROFIT_CONVERTER = "com.squareup.retrofit2:converter-gson:${Versions.RETROFIT_VERSION}"
    const val RETROFIT_OKHTTP = "com.squareup.okhttp3:okhttp:${Versions.RETROFIT_OKHTTP_VERSION}"
    const val RETROFIT_INTERCEPTOR = "com.squareup.okhttp3:logging-interceptor:${Versions.RETROFIT_OKHTTP_VERSION}"
    /** GOOGLE */
    const val MATERIAL_COMPONENTS =
        "com.google.android.material:material:${Versions.MATERIAL_COMPONENTS_VERSION}"
    /** LIFECYCLE */
    const val LIFECYCLE_VIEW_MODEL =
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.LIFECYCLE_VERSION}"
    const val LIFECYCLE_LIVE_DATA =
        "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.LIFECYCLE_VERSION}"
    const val LIFECYCLE_EXTENSIONS = "androidx.lifecycle:lifecycle-extensions:${Versions.LIFECYCLE_VERSION}"
    const val LIFECYCLE_COMMON = "androidx.lifecycle:lifecycle-common:${Versions.LIFECYCLE_VERSION}"
    const val LIFECYCLE_RUNTIME  = "androidx.lifecycle:lifecycle-runtime:${Versions.LIFECYCLE_VERSION}"
    const val LIFECYCLE_ARCH_EXTENSIONS = "android.arch.lifecycle:extensions:${Versions.LIFECYCLE_VERSION}"
    /** COROUTINES */
    const val KOTLIN_COROUTINES =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.KOTLIN_COROUTINES_VERSION}"
    // const val COROUTINES_ANDROID = "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.5"
    /** COIL */
    const val COIL = "io.coil-kt:coil:${Versions.COIL_VERSION}"
    const val COIL_SVG = "io.coil-kt:coil-svg:${Versions.COIL_VERSION}"
    /** LOTTIE */
    const val LOTTIE = "com.airbnb.android:lottie:${Versions.LOTTIE_VERSION}"
    /** ANDROID TESTING LIBS */
    const val ANDROIDX_TEST_RULES = "androidx.test:rules:${Versions.ANDROIDX_TEST_VERSION}"
    const val ANDROIDX_TEST_RUNNER = "androidx.test:runner:${Versions.ANDROIDX_TEST_VERSION}"
    const val ANDROIDX_TEST_EXT_JUNIT = "androidx.test.ext:junit:${Versions.ANDROIDX_TEST_EXT_VERSION}"
    const val ESPRESSO_CORE = "androidx.test.espresso:espresso-core:${Versions.ESPRESSO_CORE_VERSION}"
    /** TESTING LIBS */
    const val JUNIT = "junit:junit:${Versions.JUNIT_VERSION}"
    const val NAVIGATION_TESTING = "androidx.navigation:navigation-testing:${Versions.NAVIGATION_VERSION}"
    /** SAFE ARGS */
    const val SAFE_ARGS = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.NAVIGATION_VERSION}"
    /** SNACKBAR */
    const val SNACKBAR = "com.github.onurkagan:KSnack:${Versions.SNACKBAR_VERSION}"
    /** KOIN */
    const val KOIN_ANDROID = "org.koin:koin-android:${Versions.KOIN_VERSION}"
    const val KOIN_VIEWMODEL = "org.koin:koin-androidx-viewmodel:${Versions.KOIN_VERSION}"
    /** ROOM */
    const val ROOM_RUNTIME = "androidx.room:room-runtime:${Versions.ROOM_VERSION}"
    const val ROOM_COMPILER = "androidx.room:room-compiler:${Versions.ROOM_VERSION}"
    const val ROOM_KTX_EXTENSIONS = "androidx.room:room-ktx:${Versions.ROOM_VERSION}"
    /** */
    const val GLIDE = "com.github.bumptech.glide:glide:4.12.0"
    const val GLIDE_COMPLIDER = "com.github.bumptech.glide:compiler:4.12.0"

}

/** */
object URL {
    const val JITPACK_IO = "https://jitpack.io"
}