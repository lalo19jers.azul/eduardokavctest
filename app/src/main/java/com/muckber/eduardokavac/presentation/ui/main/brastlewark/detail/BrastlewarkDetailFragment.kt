package com.muckber.eduardokavac.presentation.ui.main.brastlewark.detail

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import coil.load
import com.muckber.eduardokavac.R
import com.muckber.eduardokavac.data.model.Brastlewark
import com.muckber.eduardokavac.databinding.BrastlewarkDetailFragmentBinding
import com.muckber.eduardokavac.presentation.common.extensions.loadImageWithGlide

class BrastlewarkDetailFragment : Fragment() {

    /** */
    private lateinit var viewModel: BrastlewarkDetailViewModel

    /** */
    private val binding: BrastlewarkDetailFragmentBinding
            by lazy { BrastlewarkDetailFragmentBinding.inflate(layoutInflater) }

    /** */
    private val args: BrastlewarkDetailFragmentArgs by navArgs()

    /** */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(BrastlewarkDetailViewModel::class.java)
        setUpView(args.brastlewark)
    }

    private fun setUpView(brastlewark: Brastlewark) {
        binding.apply {
            ivThumb.load(brastlewark.thumbnail)
            tvName.text = brastlewark.name
            tvAge.text = brastlewark.age.toString()
            tvHairColor.text = brastlewark.hair_color
            tvHeight.text = brastlewark.height.toString()
            tvWeight.text = brastlewark.weight.toString()
        }
    }

}