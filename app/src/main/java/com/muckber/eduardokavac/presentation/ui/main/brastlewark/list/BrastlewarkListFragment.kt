package com.muckber.eduardokavac.presentation.ui.main.brastlewark.list

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.muckber.eduardokavac.data.model.Brastlewark
import com.muckber.eduardokavac.data.model.ResponseBrastlewark
import com.muckber.eduardokavac.data.model.utils.Status
import com.muckber.eduardokavac.data.remote.api.ApiServiceImpl
import com.muckber.eduardokavac.data.remote.api.RetrofitBuilder
import com.muckber.eduardokavac.databinding.BrastlewarkListFragmentBinding
import com.muckber.eduardokavac.presentation.common.extensions.hideLoader
import com.muckber.eduardokavac.presentation.common.extensions.navigateToDestination
import com.muckber.eduardokavac.presentation.common.extensions.showLoader
import com.muckber.eduardokavac.presentation.common.extensions.showMessage
import com.muckber.eduardokavac.presentation.ui.main.brastlewark.list.adapter.BrastlewarkAdapter
import timber.log.Timber

class BrastlewarkListFragment : Fragment() {

    /** */
    private val binding: BrastlewarkListFragmentBinding
            by lazy { BrastlewarkListFragmentBinding.inflate(layoutInflater) }

    /** */
    private lateinit var viewModel: BrastlewarkListViewModel

    /** */
    private val brastlewarkAdapter: BrastlewarkAdapter
            by lazy { BrastlewarkAdapter(onRowClick = onRowClick) }

    /** */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    /** */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(
            this,
            BrastlewarkViewModelFactory(ApiServiceImpl(RetrofitBuilder.apiService))
        ).get(BrastlewarkListViewModel::class.java)

        setupObservers()
    }

    /** */
    private fun setupObservers() {
        binding.run {
            viewModel.getBrastlewark().observe(viewLifecycleOwner, {
                it?.let { resource ->
                    when (resource.status) {
                        Status.LOADING -> showLoader()
                        Status.ERROR -> {
                            hideLoader()
                            requireContext().showMessage(resource.message ?: "An error ocurred!")
                        }
                        Status.SUCCESS -> {
                            resource.data?.let { response -> initRecycler(response) }
                        }
                    }
                }
            })
        }

    }

    /** */
    private fun initRecycler(response: ResponseBrastlewark) {
        brastlewarkAdapter.submitList(response.brastlewark.take(20))
        binding.apply {
            rvBrastlewark.run {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = brastlewarkAdapter
            }
            hideLoader()
        }
    }

    /**
     * Handle on content interaction.
     */
    private val onRowClick: (Brastlewark) -> Unit = {
        val action =
            BrastlewarkListFragmentDirections.actionBrastlewarkListFragmentToBrastlewarkDetailFragment(
                brastlewark = it
            )
       findNavController().navigate(action)
    }

}