package com.muckber.eduardokavac.presentation.ui.main.brastlewark.list.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.muckber.eduardokavac.data.model.Brastlewark

class BrastlewarkAdapter(
    private val onRowClick: (Brastlewark) -> Unit
) : ListAdapter<Brastlewark, BrastlewarkViewHolder>(SongDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BrastlewarkViewHolder =
        BrastlewarkViewHolder.from(parent)


    override fun onBindViewHolder(holder: BrastlewarkViewHolder, position: Int) {
        val brastlewark = getItem(position)
        holder.bind(brastlewark = brastlewark, onClick = onRowClick)
    }

    internal class SongDiffUtil : DiffUtil.ItemCallback<Brastlewark>() {
        override fun areItemsTheSame(
            oldItem: Brastlewark,
            newItem: Brastlewark
        ): Boolean = oldItem == newItem

        override fun areContentsTheSame(
            oldItem: Brastlewark,
            newItem: Brastlewark
        ): Boolean = oldItem == newItem
    }
}