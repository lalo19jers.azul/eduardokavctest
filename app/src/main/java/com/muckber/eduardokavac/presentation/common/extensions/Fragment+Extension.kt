package com.muckber.eduardokavac.presentation.common.extensions

import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.muckber.eduardokavac.R

/* */
val progressDialog =
        ProgressDialog()

/** */
fun Fragment.showLoader() = progressDialog.showDialog(requireContext())

/** */
fun Fragment.hideLoader() = progressDialog.dialog.dismiss()

/** */
fun Fragment.navigateToDestination(resource: Int) =
        findNavController().navigate(resource)

/** */
fun Fragment.showAlert(message: CharSequence) {
        val builder = AlertDialog.Builder(requireContext())
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton("Ok") { dialog, _ ->
                        dialog.dismiss()
                }

        val dialog: AlertDialog = builder.create()
        dialog.show()
}