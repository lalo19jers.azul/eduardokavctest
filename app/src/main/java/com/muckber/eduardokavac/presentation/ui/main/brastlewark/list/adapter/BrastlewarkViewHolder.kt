package com.muckber.eduardokavac.presentation.ui.main.brastlewark.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muckber.eduardokavac.data.model.Brastlewark
import com.muckber.eduardokavac.databinding.ItemBrastlewarkBinding

class BrastlewarkViewHolder(
    private val binding: ItemBrastlewarkBinding
) :
    RecyclerView.ViewHolder(binding.root) {

    /** */
    fun bind(brastlewark: Brastlewark, onClick: (Brastlewark) -> Unit) {
        binding.apply {
           tvBrastlewark.text = brastlewark.name
            root.setOnClickListener { onClick(brastlewark) }
        }
    }

    /**
     *
     */
    companion object {
        fun from(parent: ViewGroup): BrastlewarkViewHolder {
            val layoutInflater = ItemBrastlewarkBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
            return BrastlewarkViewHolder(layoutInflater)
        }
    }
}