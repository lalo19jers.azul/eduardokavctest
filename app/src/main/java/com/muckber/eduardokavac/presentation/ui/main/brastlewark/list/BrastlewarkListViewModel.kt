package com.muckber.eduardokavac.presentation.ui.main.brastlewark.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.muckber.eduardokavac.data.model.utils.Resource
import com.muckber.eduardokavac.data.remote.repository.KavacRepository
import kotlinx.coroutines.Dispatchers

class BrastlewarkListViewModel(
    private val kavacRepository: KavacRepository
) : ViewModel() {

    /** */
    fun getBrastlewark()= liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
             emit(Resource.success(data =  kavacRepository.getBrastlewark()))
        } catch (exception: Exception) {
             emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
}