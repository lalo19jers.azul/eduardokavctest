package com.muckber.eduardokavac.presentation.ui.splash

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.muckber.eduardokavac.databinding.ActivitySplashBinding
import com.muckber.eduardokavac.presentation.common.extensions.navigateTo
import com.muckber.eduardokavac.presentation.ui.main.MainActivity

class SplashActivity : AppCompatActivity() {

    //region INIT VALUES
    private val binding: ActivitySplashBinding
            by lazy { ActivitySplashBinding.inflate(layoutInflater) }

    //endregion

    //region LIFECYCLE METHODS
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        doStuffDelay()
    }

    //endregion

    //region METHODS
    /** */
    private fun doStuffDelay() {
        Handler().postDelayed({
            navigateToMain()
        }, DELAY_TRANSITION_TIME)
    }

    /** */
    private fun navigateToMain() {
        navigateTo(MainActivity::class.java, clearTop = true)
    }

    //endregion

    //region COMPANION OBJECTS

    companion object {
        const val DELAY_TRANSITION_TIME = 3000L
    }

    //endregion
}