package com.muckber.eduardokavac.presentation.common.extensions

import android.widget.ImageView
import androidx.fragment.app.Fragment
import coil.load
import com.bumptech.glide.Glide
import com.muckber.eduardokavac.R

/** */
fun ImageView.loadImageWithGlide(fragment: Fragment, url: String) =
    Glide
        .with(fragment)
        .load(url)
        .centerCrop()
        .placeholder(R.drawable.kavak)
        .into(this)
