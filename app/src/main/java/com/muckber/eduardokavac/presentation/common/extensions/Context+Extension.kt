package com.muckber.eduardokavac.presentation.common.extensions

import android.content.Context
import android.content.Intent
import android.widget.Toast
import java.util.logging.Handler


/** */
fun <T> Context.navigateTo(javaClass: Class<T>, clearTop: Boolean = false){
    Intent(this, javaClass).apply {
        if(clearTop)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(this)
    }
}

/** */
fun Context.showMessage(message: String) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()