package com.muckber.eduardokavac.presentation.ui.main.brastlewark.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.muckber.eduardokavac.data.remote.api.ApiServiceImpl
import com.muckber.eduardokavac.data.remote.repository.KavacRepository

class BrastlewarkViewModelFactory(
    private val apiServiceImpl: ApiServiceImpl
): ViewModelProvider.Factory {

    /** */
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BrastlewarkListViewModel::class.java))
            return  BrastlewarkListViewModel(KavacRepository(apiServiceImpl = apiServiceImpl)) as T
        throw IllegalArgumentException("Unknown class name")
    }
}