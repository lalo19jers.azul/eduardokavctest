package com.muckber.eduardokavac.presentation.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.muckber.eduardokavac.R
import com.muckber.eduardokavac.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    /* */
    private val binding: ActivityMainBinding
            by lazy { ActivityMainBinding.inflate(layoutInflater) }

    /* */
    private lateinit var navController: NavController

    /** */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.activity_main_navHost) as NavHostFragment
        navController = navHostFragment.navController

        setupToolbar()
    }

    /** */
    private fun setupToolbar() {
        binding.toolbar.setupWithNavController(navController)
    }

}