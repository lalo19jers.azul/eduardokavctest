package com.muckber.eduardokavac.presentation.common.extensions

import android.view.View

/** */
fun View?.visible() {
    this?.visibility = View.VISIBLE
}

/** */
fun View?.gone() {
    this?.visibility = View.GONE
}