package com.muckber.eduardokavac.data.model.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}