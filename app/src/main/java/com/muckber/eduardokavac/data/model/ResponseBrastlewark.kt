package com.muckber.eduardokavac.data.model

import com.google.gson.annotations.SerializedName


data class ResponseBrastlewark(
    @SerializedName("Brastlewark")
    val brastlewark: List<Brastlewark>
)