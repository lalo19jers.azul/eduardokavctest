package com.muckber.eduardokavac.data.remote.api



class ApiServiceImpl(private val apiService: ApiService) {

    /** */
    suspend fun getBrastlewark() =  apiService.getBrastlewark()

}