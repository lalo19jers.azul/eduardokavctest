package com.muckber.eduardokavac.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Brastlewark(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("thumbnail")
    val thumbnail: String,
    @SerializedName("age")
    val age: Int,
    @SerializedName("weight")
    val weight: Double,
    @SerializedName("height")
    val height: Double,
    @SerializedName("hair_color")
    val hair_color: String,
    @SerializedName("professions")
    val professions: List<String>,
    @SerializedName("friends")
    val friends: List<String>,
): Parcelable