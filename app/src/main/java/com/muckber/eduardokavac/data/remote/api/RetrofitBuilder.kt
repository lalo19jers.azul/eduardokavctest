package com.muckber.eduardokavac.data.remote.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {

    /** */
    private fun initRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(ApiConstants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    /** */
    val apiService: ApiService = initRetrofit().create(ApiService::class.java)
}