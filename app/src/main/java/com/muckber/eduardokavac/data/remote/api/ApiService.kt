package com.muckber.eduardokavac.data.remote.api

import com.muckber.eduardokavac.data.model.Brastlewark
import com.muckber.eduardokavac.data.model.ResponseBrastlewark
import retrofit2.http.*


interface ApiService {

    /** */
    @GET(ApiConstants.GET_BRASTLEWARK)
    suspend fun getBrastlewark(): ResponseBrastlewark

}