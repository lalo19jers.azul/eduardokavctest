package com.muckber.eduardokavac.data.remote.repository

import com.muckber.eduardokavac.data.remote.api.ApiServiceImpl

class KavacRepository(private val apiServiceImpl: ApiServiceImpl) {

    /** */
    suspend fun getBrastlewark() = apiServiceImpl.getBrastlewark()

}