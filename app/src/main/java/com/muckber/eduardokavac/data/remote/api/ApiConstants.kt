package com.muckber.eduardokavac.data.remote.api

object ApiConstants {

    // Endpoints
    const val BASE_URL = "https://raw.githubusercontent.com/rrafols/mobile_test/"
    const val GET_BRASTLEWARK = "master/data.json"

}
