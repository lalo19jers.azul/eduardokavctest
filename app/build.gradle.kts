
plugins {
    id ("com.android.application")
    id ("kotlin-android")
    id("kotlin-parcelize")
    id("androidx.navigation.safeargs.kotlin")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(Sdk.COMPILE_SDK_VERSION)
    buildToolsVersion(Build.BUILD_TOOL_VERSION)

    defaultConfig {
        minSdkVersion(Sdk.MIN_SDK_VERSION)
        targetSdkVersion(Sdk.TARGET_SDK_VERSION)
        applicationId = AppCoordinates.APP_ID
        versionCode = AppCoordinates.APP_VERSION_CODE
        versionName = AppCoordinates.APP_VERSION_NAME
        multiDexEnabled = true

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            resValue("string", "app_name_res", "Bitware Production")
        }

    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    buildFeatures {
        dataBinding = true
    }
}

dependencies {

    /** KOTLIN JETBRAINS */
    implementation(Dependencies.JET_BRAINS_KOTLIN)
    /** ANDROID X */
    implementation(Dependencies.ANDROIDX_CORE_KTX)
    implementation(Dependencies.ANDROIDX_APPCOMPAT)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.LEGACY_SUPPORT)
    /** NAVIGATION */
    implementation(Dependencies.NAVIGATION_FRAGMENT_KTX)
    implementation(Dependencies.NAVIGATION_UI_KTX)
    implementation(Dependencies.NAVIGATION_FEATURES_FRAGMENT)
    /** DEBUG */
    implementation(Dependencies.TIMBER)
    /** GOOGLE */
    implementation(Dependencies.MATERIAL_COMPONENTS)
    /** LIFECYCLE */
    implementation(Dependencies.LIFECYCLE_VIEW_MODEL)
    implementation(Dependencies.LIFECYCLE_LIVE_DATA)
    implementation(Dependencies.LIFECYCLE_EXTENSIONS)
    implementation(Dependencies.LIFECYCLE_COMMON)
    implementation(Dependencies.LIFECYCLE_RUNTIME)
    implementation(Dependencies.LIFECYCLE_ARCH_EXTENSIONS)
    /** COIL */
    implementation(Dependencies.COIL)
    implementation(Dependencies.COIL_SVG)
    /** KOIN */
    implementation(Dependencies.KOIN_ANDROID)
    implementation(Dependencies.KOIN_VIEWMODEL)
    /** LOTTIE */
    implementation(Dependencies.LOTTIE)
    /** RETROFIT */
    implementation(Dependencies.RETROFIT)
    implementation(Dependencies.RETROFIT_CONVERTER)
    implementation(Dependencies.RETROFIT_INTERCEPTOR)
    implementation(Dependencies.RETROFIT_OKHTTP)
    /** ROOM */
    implementation(Dependencies.ROOM_RUNTIME)
    kapt(Dependencies.ROOM_COMPILER)
    implementation(Dependencies.ROOM_KTX_EXTENSIONS)
    /** GLIDE */
    implementation(Dependencies.GLIDE)
    annotationProcessor(Dependencies.GLIDE_COMPLIDER)
    /** TESTING */
    testImplementation(Dependencies.JUNIT)
    androidTestImplementation(Dependencies.ANDROIDX_TEST_EXT_JUNIT)
    androidTestImplementation(Dependencies.ESPRESSO_CORE)
    androidTestImplementation(Dependencies.NAVIGATION_TESTING)

}